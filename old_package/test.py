from bivariate.class_dataset import *
import pandas as pd

dataset = Dataset.import_from_filename()

Dataset.time_plot()
Dataset.hist_plot()
pd.DataFrame()