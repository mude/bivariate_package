class Comparison():
    """
    Carry out a comparison of two or more Distribution objects, either as 
    plots or calculated values. For example, plots of PDF's for several 
    distributions. Tables include values of PDF's or CDF's, but also can 
    compare moments, distribution parameters, etc.
    
    An instance of this class is created by providing a list of distributions 
    (instances of the Distribution class).
    
    Start with plots, since that's in bivariate package already. 
    Tables can come later
    
    """
    def __init__(self, distributions):
        """
        Attributes will include some summary information about the 
        distributions that will be compared, mostly filled in from 
        distribution attributes.
        """
        pass
    
    def pdf():
        pass
    
    def cdf():
        pass